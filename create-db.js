// Switch to new DB
use mern-blog-db;

// Delete all
db.articles.deleteMany({});

// Add document to DB
db.articles.insert([
  {
    name:     'learn-react',
    upvotes:  0,
    comments: []
  },
  {
    name:     'learn-node',
    upvotes:  0,
    comments: []
  },
  {
    name:     'my-thoughts-on-resumes',
    upvotes:  0,
    comments: []
  }
]);

// Search all
db.articles.find({}).pretty();
