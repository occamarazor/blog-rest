#!/bin/bash

# Install XCode
xcode-select --install

# Install Homebrew
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
# check for problems
brew doctor
# add homebrew to path
export PATH="/usr/local/bin:$PATH"
# update
brew update
# upgrade packages and casks
brew upgrade
# uninstall old versions
brew cleanup

# Install nvm
brew install nvm
mkdir ~/.nvm
# edit shell profile
open -a TextEdit ~/.bash_profile
  # export NVM_DIR=~/.nvm
  # source $(brew --prefix nvm)/nvm.sh
source ~/.bash_profile
echo $NVM_DIR

# Install node.js
nvm install 12

# Tap the MongoDB Homebrew Tap
brew tap mongodb/brew

# Install MongoDB
brew install mongodb-community@4.2

# 1. Run MongoDB as a macOS service (recommended)
brew services start mongodb-community@4.2

# 2. Run MongoDB manually as a background process
#mongod --config /usr/local/etc/mongod.conf --fork

# Verify process running
ps aux | grep -v grep | grep mongod

# Create DB
mongo < create-db.js

# Install project dependencies
npm i
