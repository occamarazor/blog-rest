import path            from 'path';
import express         from 'express';
import bodyParser      from 'body-parser';
import { MongoClient } from 'mongodb';

const app = express();

// Static files location
app.use(express.static(path.join(__dirname, 'spa')));

// 1. Parses JSON included with the POST request
// 2. Adds [body] prop to the [req] param of matching route
app.use(bodyParser.json());

const withDB = async(operations, res) => {
  try {
    const client = await MongoClient.connect('mongodb://localhost:27017', { useUnifiedTopology: true });
    const db = client.db('mern-blog-db');
    
    await operations(db);
    client.close();
  } catch(error) {
    res.status(500).json({ message: 'DB connection error', error });
  }
};

// GET http://localhost:8000/api/articles/learn-node
app.get('/api/articles/:name', async(req, res) => {
  withDB(async(db) => {
    const articleName = req.params.name;
    const articleInfo = await db.collection('articles').findOne({ name: articleName });
    res.status(200).json(articleInfo);
  }, res);
});

// POST http://localhost:8000/api/articles/learn-node/upvote
app.post('/api/articles/:name/upvote', async(req, res) => {
  withDB(async(db) => {
    const articleName = req.params.name;
    const articleInfo = await db.collection('articles').findOne({ name: articleName });
    
    await db.collection('articles').updateOne(
      { name: articleName },
      { '$set': { upvotes: articleInfo.upvotes + 1 } }
      // { '$set': { upvotes: 0 } } // reset upvotes
    );
    
    const updatedArticleInfo = await db.collection('articles').findOne({ name: articleName });
    
    res.status(200).json(updatedArticleInfo);
  }, res);
});

// POST http://localhost:8000/api/articles/learn-react/comment
// Body JSON
// { "username": "Wakiki", "text": "I love this stuff!" }
app.post('/api/articles/:name/comment', (req, res) => {
  withDB(async(db) => {
    const articleName = req.params.name;
    const userComment = req.body;
    const articleInfo = await db.collection('articles').findOne({ name: articleName });
    
    await db.collection('articles').updateOne(
      { name: articleName },
      { '$set': { comments: [ ...articleInfo.comments, userComment ] } }
      // { '$set': { comments: [] } } // reset comments
    );
    
    const updatedArticleInfo = await db.collection('articles').findOne({ name: articleName });
    
    res.status(200).json(updatedArticleInfo);
  }, res);
});


// Pass on all other requests to SPA
app.get('*', (req, res) => {
  res.sendFile(path.join(`${__dirname}/spa/index.html`));
});

app.listen(8000, () => console.log('Listening on port 8000...'));
