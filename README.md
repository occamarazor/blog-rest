# MERN Blog Demo

###### Overview: Read articles, upvote & leave comments.

**View demo: [mern-blog.amazonaws.com](http://ec2-3-15-217-222.us-east-2.compute.amazonaws.com/)**   

## Runbook
#### Set Dev Environment (macOS)  
```
chmod u+x create-env.sh
bash create-env.sh
```

#### Run locally
```
npm start
```

#### Deploy on AWS
- Launch EC2 instance
- Select Amazon Linux 2 AMI
- Review and launch with defaults
- Launch
- Create a new key pair
- Download key pair
- Launch instances
- Move key pair to ssh dir  
```
cd
mkdir .ssh
mv Downloads/%KEY_PAIR%.pem ~/.ssh/%KEY_PAIR%.pem
```
- Change permissions to key pair  
```
chmod 400 ~/.ssh/%KEY_PAIR%.pem
```
- Copy EC2 instance's public DNS
- SSH into EC2 instance  
```
ssh -i ~/.ssh/%KEY_PAIR%.pem ec2-user@%PUBLIC_DNS%
```
- Install git  
```
sudo yum install git
```
- Install nvm  
```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
```
- Activate nvm  
```
. ~/.nvm/nvm.sh
```
- Install node.js  
```
nvm install 12
```
- Install latest npm  
```
npm install -g npm@latest
```
- Configure package management system  
```
# Create a file 
sudo nano /etc/yum.repos.d/mongodb-org-4.2.repo

## Add content
# [mongodb-org-4.2]
# name=MongoDB Repository
# baseurl=https://repo.mongodb.org/yum/amazon/2/mongodb-org/4.2/x86_64/
# gpgcheck=1
# enabled=1
# gpgkey=https://www.mongodb.org/static/pgp/server-4.2.asc
```

- Install MongoDB packages   
```
sudo yum install -y mongodb-org
```
- Start MongoDB server  
```
sudo service mongod start
```
- Clone git repo  
```
git clone https://Maxcrank@bitbucket.org/Maxcrank/mern-blog-rest.git
```
- Create DB  
```
mongo < mern-blog-rest/create-db.js
```
- Install project dependencies  
```
cd mern-blog-rest
npm i
```
- Install forever  
```
npm i -g forever
```
- Run and check app status  
```
forever start -c 'npm start' .
forever list
```
- Map port 80 on AWS instance to port 8000 on node server  
```
sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-ports 8000
```
- In security groups  
    - Select corresponding group
    - Select inbound tab
    - Edit
    - Add rule HTTP/Anywhere

## What's inside
1. React SPA build
2. Node
3. Express
4. MongoDB
5. AWS

## TODO
- Switch to mobile first design
- Switch to CSS modules & SCSS
- Add animation
- Hook up a domain
- Add user authentication
- Secure DB
- Improve error handling
- Write unit tests
- Add content
